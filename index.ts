import fs from 'fs';
import { Omnivore } from '@omnivore-app/api';
import { transform } from './transform';
import path from 'path';

const exp = async (folder: string) => {
  if (!process.env.OMNIVORE_API_KEY) {
    throw new Error('OMNIVORE_API_KEY environment variable is missing');
  }
  const omnivore = new Omnivore({ apiKey: process.env.OMNIVORE_API_KEY });

  const result = await omnivore.items.search({ query: 'label:Logseq' });
  const files = result.edges.map((edge) => edge.node).map(transform);

  const omnivoreFolder = path.join(folder, 'omnivore');

  fs.mkdirSync(omnivoreFolder, { recursive: true });
  files.forEach((file) => {
    const filePath = path.join(omnivoreFolder, file.filename);
    fs.writeFileSync(filePath, file.content);
  });
};

if (!process.argv[2]) {
  throw new Error('Please provide a folder path');
}

exp(process.argv[2]);
