import { Highlight, Item } from '@omnivore-app/api';

const sortByCreatedAtAsc = (a: any, b: any): number => {
  // string in a format '2024-05-26T16:39:30.000Z'
  const aDate = new Date(a.createdAt || a.updatedAt).valueOf();
  // string in a format '2024-05-26T16:39:30.000Z'
  const bDate = new Date(b.createdAt || b.updatedAt).valueOf();
  return aDate < bDate ? -1 : aDate === bDate ? 0 : 1;
};

const sanitize = (title: string): string => {
  return title.replace(/[^a-zA-Z0-9 ]/g, '-');
};

const transformHighlight = (highlight: Highlight): string => {
  if (!highlight.quote) {
    console.error(
      `surprising, the following highlight has no quote:\n${JSON.stringify(
        highlight
      )}`
    );
  }
  const quote = highlight
    .quote!.split('\n')
    .map((line) => `> ${line}\n`)
    .join('');

  const note = highlight.annotation;

  if (!note) {
    return quote;
  }
  return `${quote}\n${note}\n`;
};

const convertDate = (dateString: string): string => {
  const date = new Date(dateString);
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const day = String(date.getDate()).padStart(2, '0');
  return `[[${year}-${month}-${day}]]`;
};

export const transform = (
  item: Item
): { filename: string; content: string } => {
  const filename = `${sanitize(item.title)}.md`;
  const content = `# ${item.title}

- #omnivore
- [article link](${item.originalArticleUrl})
- read on ${convertDate(item.updatedAt || item.savedAt)}
${item.publishedAt ? `- published on ${convertDate(item.publishedAt)}` : ''}

${item.highlights
  ?.sort(sortByCreatedAtAsc)
  .map(transformHighlight)
  .join('\n')}`;
  return { filename, content };
};
