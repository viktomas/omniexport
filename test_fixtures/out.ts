export const response = {
  __typename: 'SearchSuccess',
  edges: [
    {
      node: {
        id: '4b83959c-ce8a-4e53-82c2-b5a473ed1e48',
        title: "Feynman's Garden @ marginalia.nu",
        siteName: 'marginalia.nu',
        originalArticleUrl:
          'https://www.marginalia.nu/log/a_108_feynman_revisited/',
        author: null,
        description:
          'The best description of my problem solving process is the Feynman algorithm, which is sometimes presented as a joke where the hidden subtext is “be smart”, but I disagree. The “algorithm” is a surprisingly lucid description of how thinking works in the context of hard problems where the answer can’t simply be looked up or trivially broken down, iterated upon in a bottom-up fashion, or approached with similar methods.\nFeynman’s thinking algorithm is described like this:',
        slug: 'https-www-marginalia-nu-log-a-108-feynman-revisited-18fb5c29d32',
        labels: [
          {
            name: 'Logseq',
            color: '#0693E3',
            createdAt: '2024-04-21T19:50:27.000Z',
            id: '66f3813e-0018-11ef-b697-0350db3e05ab',
            internal: false,
            source: null,
            description: 'export to logseq',
          },
        ],
        highlights: [
          {
            id: '99e0cb69-1796-4020-b580-2fc27fa43d2d',
            quote:
              '_The conclusions must be written down_. Not only because these insights are fleeting and can be hard to recall, it also appears to act as a sort of feedback to the brain that the information is important',
            annotation:
              'This is not the first time I\'m hearing about "rewarding" the ideas by writing them down',
            patch:
              '@@ -2580,24 +2580,44 @@\n derstanding.\n+%3Comnivore_highlight%3E\n The conclusi\n@@ -2805,16 +2805,37 @@\n mportant\n+%3C/omnivore_highlight%3E\n , that i\n',
            updatedAt: '2024-05-26T16:39:30.000Z',
            labels: [],
            type: 'HIGHLIGHT',
            highlightPositionPercent: 0,
            color: null,
            highlightPositionAnchorIndex: 0,
            prefix: null,
            suffix: null,
            createdAt: '2024-05-26T16:39:30.000Z',
          },
          {
            id: 'a71b1c31-3a1c-46ec-aece-da4247b4a810',
            quote:
              'Thinking is a background process. There’s no Sherlock Holmes-style string of brilliant deductions; the brain is a connection-making machine. If you feed it data points, it will find ways of connecting the dots.',
            annotation: null,
            patch:
              '@@ -985,16 +985,36 @@\n  fed it.\n+%3Comnivore_highlight%3E\n Thinking\n@@ -1215,16 +1215,37 @@\n he dots.\n+%3C/omnivore_highlight%3E\n So to so\n',
            updatedAt: '2024-05-26T16:36:39.000Z',
            labels: [],
            type: 'HIGHLIGHT',
            highlightPositionPercent: 0,
            color: null,
            highlightPositionAnchorIndex: 0,
            prefix: null,
            suffix: null,
            createdAt: '2024-05-26T16:36:39.000Z',
          },
          {
            id: 'ac823d52-7297-4735-9529-23605624e1cf',
            quote:
              'Imagine if you were to task someone else with solving the problem, imagine what they might have use of knowing, really go over anything you think might be useful. It’s good to write that down in the simplest way possible. It should be very clear to you what question you are attempting to answer.',
            annotation:
              'I should try this, spend more time documenting before going to solution mode ',
            patch:
              '@@ -1403,16 +1403,36 @@\n elevant.\n+%3Comnivore_highlight%3E\n Imagine \n@@ -1719,16 +1719,37 @@\n  answer.\n+%3C/omnivore_highlight%3E\n  What is\n',
            updatedAt: '2024-05-26T16:37:41.000Z',
            labels: [],
            type: 'HIGHLIGHT',
            highlightPositionPercent: 0,
            color: null,
            highlightPositionAnchorIndex: 0,
            prefix: null,
            suffix: null,
            createdAt: '2024-05-26T16:36:53.000Z',
          },
          {
            id: '01bfc9be-cdf3-4daa-b8f8-6c038b7442bc',
            quote:
              'it’s crucial to step away from the keyboard. Go for a walk, or sit down in a quiet place, take a shower; whatever you choose to do, just keep thinking about the problem.',
            annotation: null,
            patch:
              '@@ -2083,16 +2083,36 @@\n reason, \n+%3Comnivore_highlight%3E\n it%E2%80%99s cru\n@@ -2272,16 +2272,37 @@\n problem.\n+%3C/omnivore_highlight%3E\n As you d\n',
            updatedAt: '2024-05-26T16:38:22.000Z',
            labels: [],
            type: 'HIGHLIGHT',
            highlightPositionPercent: 0,
            color: null,
            highlightPositionAnchorIndex: 0,
            prefix: null,
            suffix: null,
            createdAt: '2024-05-26T16:38:22.000Z',
          },
          {
            id: '467b4d3f-9c4f-4f77-99a5-dc570ca82e8d',
            quote:
              'What you think today is the result of what you read yesterday. If you want to have relevant, interesting, useful thoughts; use your reading as a template.',
            annotation: null,
            patch:
              '@@ -3492,16 +3492,36 @@\n thought.\n+%3Comnivore_highlight%3E\n What you\n@@ -3666,16 +3666,37 @@\n emplate.\n+%3C/omnivore_highlight%3E\n  It will\n',
            updatedAt: '2024-05-26T16:40:28.000Z',
            labels: [],
            type: 'HIGHLIGHT',
            highlightPositionPercent: 0,
            color: null,
            highlightPositionAnchorIndex: 0,
            prefix: null,
            suffix: null,
            createdAt: '2024-05-26T16:40:28.000Z',
          },
          {
            id: 'f8f157cb-3e71-43f3-84fc-5ef6034eba3c',
            quote:
              'you do next is to remove yourself from further inputs. As long as you keep feeding your brain with more data, the queries never resolve, and eventually the stuff you fed it first will fall out of the context window and amount to nothing.',
            annotation: null,
            patch:
              '@@ -1825,24 +1825,44 @@\n stion, what \n+%3Comnivore_highlight%3E\n you do next \n@@ -2086,16 +2086,37 @@\n nothing.\n+%3C/omnivore_highlight%3E\n For this\n',
            updatedAt: '2024-05-26T16:38:10.000Z',
            labels: [],
            type: 'HIGHLIGHT',
            highlightPositionPercent: 0,
            color: null,
            highlightPositionAnchorIndex: 0,
            prefix: null,
            suffix: null,
            createdAt: '2024-05-26T16:38:10.000Z',
          },
          {
            id: 'e38ad15c-d9df-475f-b71b-d5e205a7b5d8',
            quote:
              'I’d grind away and work on the search engine all week, filling my brain with all manner of information related to the work I was doing, and then I’d go visit her over the weekend, not bringing a computer.\n\nWithout exception, by the time the weekend was over, I’d have several pages worth of new ideas written down in a notebook',
            annotation: null,
            patch:
              '@@ -3049,16 +3049,36 @@\n m above.\n+%3Comnivore_highlight%3E\n I%E2%80%99d grin\n@@ -3394,16 +3394,37 @@\n notebook\n+%3C/omnivore_highlight%3E\n  that I%E2%80%99\n',
            updatedAt: '2024-05-26T16:39:53.000Z',
            labels: [],
            type: 'HIGHLIGHT',
            highlightPositionPercent: 0,
            color: null,
            highlightPositionAnchorIndex: 0,
            prefix: null,
            suffix: null,
            createdAt: '2024-05-26T16:39:53.000Z',
          },
        ],
        updatedAt: '2024-05-27T16:42:03.000Z',
        savedAt: '2024-05-27T16:36:22.000Z',
        pageType: 'ARTICLE',
        content:
          '<DIV id="readability-content"><DIV data-omnivore-anchor-idx="1" class="page" id="readability-page-1"><DIV data-omnivore-anchor-idx="2"><article data-omnivore-anchor-idx="3"><div data-omnivore-anchor-idx="4" id="content"><p data-omnivore-anchor-idx="5">The best description of my problem solving process is the Feynman algorithm,\nwhich is sometimes presented as a joke where the hidden subtext is “be smart”, but\nI disagree. The “algorithm” is a surprisingly lucid description of how thinking works in the\ncontext of hard problems where the answer can’t simply be looked up or trivially\nbroken down, iterated upon in a bottom-up fashion, or approached with similar methods.</p><p data-omnivore-anchor-idx="6">Feynman’s thinking algorithm is described like this:</p><ol data-omnivore-anchor-idx="7"><li data-omnivore-anchor-idx="8">Write down the problem</li><li data-omnivore-anchor-idx="9">Think real hard</li><li data-omnivore-anchor-idx="10">Write down the solution</li></ol><p data-omnivore-anchor-idx="11">(This algorithm may or may not have actually been used by Feynman, it’s as far as I can tell\nMurray Gell-Mann’s account of Feynman’s approach.)</p><p data-omnivore-anchor-idx="12">The trick is that there is no trick. This is how thinking works.\nIt appears that when you feed your brain related information, without further active involvement,\nit starts to digest the information you’ve fed it.</p><p data-omnivore-anchor-idx="13">Thinking is a background process. There’s no Sherlock Holmes-style string of brilliant deductions;\nthe brain is a connection-making machine. If you feed it data points, it will find ways of connecting\nthe dots.</p><p data-omnivore-anchor-idx="14">So to solve a problem, you start by feeding your mind the relevant information in a fairly literal sense.\nIt’s a direct parallel to how you’d prompt a LLM. Just bring up all the things you think are relevant.</p><p data-omnivore-anchor-idx="15">Imagine if you were to task someone else with solving the problem, imagine what they might have use of knowing,\nreally go over anything you think might be useful. It’s good to write that down in the simplest way possible.\nIt should be very clear to you what question you are attempting to answer. What is written on paper is not the\nimportant thing, the act of assembling the question is the work!</p><p data-omnivore-anchor-idx="16">To answer the question, what you do next is to remove yourself from further inputs.\nAs long as you keep feeding your brain with more data, the queries never resolve, and\neventually the stuff you fed it first will fall out of the context window and amount to nothing.</p><p data-omnivore-anchor-idx="17">For this reason, it’s crucial to step away from the keyboard. Go for a walk, or sit down in a quiet place,\ntake a shower; whatever you choose to do, just keep thinking about the problem.</p><p data-omnivore-anchor-idx="18">As you do this, all the background processing you kicked off earlier will begin to resolve.\nNot immediately, it may take a while—hours, sometimes days—but as it does you get sudden\ninsights into the problem domain. You may or may not immediately find a solution to the problem,\nbut you’ll often at least find a deeper understanding.</p><p data-omnivore-anchor-idx="19"><em data-omnivore-anchor-idx="20">The conclusions must be written down</em>. Not only because these insights are fleeting and\ncan be hard to recall, it also appears to act as a sort of feedback to the brain that the\ninformation is important, that it’s on the right track, to keep going.</p><hr data-omnivore-anchor-idx="21"><p data-omnivore-anchor-idx="22">The perhaps most inspired period of my life was sometime in 2021, before I’d moved\nin with my girlfriend. What I’d inadvertently ended up doing was to arrange my life\ninto something very similar to the algorithm above.</p><p data-omnivore-anchor-idx="23">I’d grind away and work on the search engine all week, filling my brain with all manner of information\nrelated to the work I was doing, and then I’d go visit her over the weekend, not bringing a computer.</p><p data-omnivore-anchor-idx="24">Without exception, by the time the weekend was over, I’d have several pages worth of new ideas written\ndown in a notebook that I’d implement over the following week, rinse and repeat.</p><hr data-omnivore-anchor-idx="25"><p data-omnivore-anchor-idx="26">I keep coming back to the <a data-omnivore-anchor-idx="27" href="https://www.marginalia.nu/log/05-minds-field/">gardening metaphor of thought</a>.</p><p data-omnivore-anchor-idx="28">What you think today is the result of what you read yesterday. If you want to have relevant,\ninteresting, useful thoughts; use your reading as a template. It will not only model the topics\nyou are thinking about, but the very thoughts themselves. It’s as much pruning gossip and\nirrelevant noise as it is planting the interesting and insightful and well thought-out.</p></div><nav data-omnivore-anchor-idx="29"><table data-omnivore-anchor-idx="30"><tbody data-omnivore-anchor-idx="31"><tr data-omnivore-anchor-idx="32"><th data-omnivore-anchor-idx="33" colspan="2">Previous:</th></tr></tbody></table></nav></article></DIV></DIV></DIV>',
        publishedAt: '2024-05-26T00:00:00.000Z',
        url: 'https://www.marginalia.nu/log/a_108_feynman_revisited/',
        image: null,
        readAt: '2024-05-26T16:42:03.000Z',
        wordsCount: 688,
        readingProgressPercent: 100,
        isArchived: false,
        archivedAt: null,
        contentReader: 'WEB',
      },
    },
  ],
  pageInfo: {
    hasNextPage: false,
    hasPreviousPage: false,
    startCursor: '',
    endCursor: '9',
    totalCount: 9,
  },
};
