import { Item } from '@omnivore-app/api';
import { response } from './test_fixtures/out';
import { transform } from './transform';

describe('transform', () => {
  const result = transform(response.edges[0].node as Item);
  it('should transform filename', () => {
    expect(result.filename).toBe(`Feynman-s Garden - marginalia-nu.md`);
  });
  it('should transform content', () => {
    console.log(result.content);
    expect(result.content.trim()).toBe(
      `
# Feynman's Garden @ marginalia.nu

- #omnivore
- [article link](https://www.marginalia.nu/log/a_108_feynman_revisited/)
- read on [[2024-05-27]]
- published on [[2024-05-26]]

> Thinking is a background process. There’s no Sherlock Holmes-style string of brilliant deductions; the brain is a connection-making machine. If you feed it data points, it will find ways of connecting the dots.

> Imagine if you were to task someone else with solving the problem, imagine what they might have use of knowing, really go over anything you think might be useful. It’s good to write that down in the simplest way possible. It should be very clear to you what question you are attempting to answer.

I should try this, spend more time documenting before going to solution mode 

> you do next is to remove yourself from further inputs. As long as you keep feeding your brain with more data, the queries never resolve, and eventually the stuff you fed it first will fall out of the context window and amount to nothing.

> it’s crucial to step away from the keyboard. Go for a walk, or sit down in a quiet place, take a shower; whatever you choose to do, just keep thinking about the problem.

> _The conclusions must be written down_. Not only because these insights are fleeting and can be hard to recall, it also appears to act as a sort of feedback to the brain that the information is important

This is not the first time I'm hearing about "rewarding" the ideas by writing them down

> I’d grind away and work on the search engine all week, filling my brain with all manner of information related to the work I was doing, and then I’d go visit her over the weekend, not bringing a computer.
> 
> Without exception, by the time the weekend was over, I’d have several pages worth of new ideas written down in a notebook

> What you think today is the result of what you read yesterday. If you want to have relevant, interesting, useful thoughts; use your reading as a template.
  
`.trim()
    );
  });
});
