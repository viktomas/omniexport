# Omniexport

This is a small script that connects to [Omnivore API](https://docs.omnivore.app/integrations/api.html) and exports your read posts with highlights.

Currently it filters for `Logseq` tag for historical reasons but it can be easily changed in `index.ts`.

## Usage

`OMNIVORE_API_KEY=<your-key> npm start <output-folder>`

This creates subfolder `omnivore` in your output-folder and populates it with one file per article.

## Run tests

`npm test`
